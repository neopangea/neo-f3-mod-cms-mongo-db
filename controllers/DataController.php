<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/11/14
 * Time: 12:58 PM
 */

namespace Neo\Mongo;

class DataController extends \Neo\F3\Controller {

    public function index( $f3, $args ) {
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_REDIRECT);
        parent::__construct( $f3 );
        $template = \Template::instance();
        echo $template->render('/vendor/neopangea/f3-mod-cms-mongo-db/templates/index.htm');
    }

    public function get( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);
        $response = new \Neo\F3\Response();

        $db = MongoDal::getDB($f3);

        // If asked for, include dump of collections
        if (isset($_GET['collections'])) {
            $response->data->collections = array();
            foreach ($_GET['collections'] as $collection) {
                $response->data->collections[$collection] = array_map(function ($document) {
                    return $document;
                }, iterator_to_array($db->$collection->find()));
            }
        }

        // Also include list of all collection names
        $response->data->collectionNames = array_map(function ($collection) {
            return $collection->getName();
        }, $db->listCollections(false));

        exit(json_encode($response));
    }

    public function uploadJson( $f3, $args ) {
        parent::__construct( $f3 );
        \Neo\Cms\Lib\Auth::adminGate(\Neo\Cms\Lib\Auth::GATE_THROW_401);

        $response = new \Neo\F3\Response();

        try {

            if(count($_FILES) < 1) {
                throw new \Exception('No file sent.');
            }

            $file = reset($_FILES);

            $json = file_get_contents($file['tmp_name']);

            $collections = json_decode($json);

            $db = MongoDal::getDB($f3);

            foreach ($collections as $collectionName => $collectionValue) {
                // remove _id keys
                $collectionValue = array_values((array)$collectionValue);
                $db->{$collectionName}->drop();
                $db->{$collectionName}->batchInsert($collectionValue);
            }

        } catch (\Exception $e) {

            $response->successful = false;
            $response->message    = $e->getMessage();
        }

        exit(json_encode($response));
    }
} 