<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 7/20/14
 * Time: 4:33 PM
 */

namespace Neo\Mongo;

/**
 * Class App
 * @package Neo
 */

abstract class MongoDal extends \Neo\F3\Data\Dal {

    /*
    protected static
        $collectionName,
        $entityClass,
        $insertMapper,
        $updateMapper;
    */

    /**
     * Gets a Mongo DB instance.
     * @return \DB\Mongo|void
     */

    public static function getDB($f3) {
        return new \DB\Mongo("mongodb://{$f3->get('NEO_DB_UNAME')}:{$f3->get('NEO_DB_PWORD')}@{$f3->get('NEO_DB_HOST')}:{$f3->get('NEO_DB_PORT')}/{$f3->get('NEO_DB_NAME')}", $f3->get('NEO_DB_NAME'));
    }

    /**
     * Abstract insert method. Never call this directly, rather, call parent DAL save(). Insert/Update logic is
     * contained within.
     * @throws \Exception if not implemented
     * @param $db may be passed in for query chaining transactions
     * @param $entity
     * @return bool
     */

    public static function insert(&$db, &$entity) {

        $document = array();

        static::$insertMapper->map($document, $entity);

        $document['updated'] = new \MongoDate();
        $document['created'] = new \MongoDate();

        if (!$db->{static::$collectionName}->insert($document)) { return false; }

        $entity->_id = $document['_id'];
        return true;
    }

    /**
     * Abstract SQL update method. Never call this directly, rather, call parent DAL save(). Insert/Update logic is
     * contained within.
     * @throws \Exception if not implemented
     * @param $entity
     * @param $db may be passed in for query chaining transactions
     * @return bool
     */

    public static function update(&$db, &$entity) {

        $document = array();

        static::$updateMapper->map($document, $entity);

        $document['updated'] = new \MongoDate();

        $filter     = array('_id' => $entity->_id);
        $updates    = array('$set' => $document);

        if (!$db->{static::$collectionName}->update($filter, $updates)) {
            return false;
        }
        return true;
    }

    /**
     * @param $data
     * @throws \Exception
     * @returns Entity
     */

    protected static function getEntity ($data) {
        return (new \ReflectionClass(static::$entityClass))->newInstanceArgs(array($data));
    }

    /**
     * @param $db
     * @param $_id
     * @return bool|Entity|void
     */

    public static function get (&$db, $_id) {
        if (is_string($_id)) { $_id = new \MongoId($_id); }
        $document   = $db->{static::$collectionName}->findOne(array('_id' => $_id));
        if (is_null($document)) { return false; }
        return static::getEntity($document);
    }

    /**
     * Default SELECT all
     * @throws \Exception if not implemented
     * @param DB $db
     * @param array $options
     * @return array|FALSE|int
     */

    public static function getAll (&$db, $options = array()) {
        $cursor = $db->{static::$collectionName}->find();
        if (is_null($cursor)) { return array(); }

        $entities = array();

        foreach (iterator_to_array($cursor) as $document) {
            $entities[] = static::getEntity($document);
        }

        return $entities;
    }

    /**
     * @param $db
     * @param $_id
     * @return mixed
     */

    public static function delete (&$db, $_id) {
        return $db->{static::$collectionName}->remove(array('_id' => new \MongoId($_id)), array("justOne" => true));
    }
}