<?php
/**
 * Created by PhpStorm.
 * User: Paul
 * Date: 9/11/14
 * Time: 1:27 PM
 */

namespace Neo\Mongo;


abstract class MongoEntity extends \Neo\F3\Entity {
    public
        $_id = null;

    public function isNew () {
        return is_null($this->_id);
    }
}