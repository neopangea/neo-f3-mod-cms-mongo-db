/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global NP, angular, window, _*/

(function () {

    'use strict';

    angular.module('dataApp').controller(
        'dataUploadController',
        [
            'config',
            '$scope',
            'neoSimpleFileUpload',
            function (config, $scope, neoSimpleFileUpload) {

                $scope.data = {
                    upload : {

                    }
                };

                $scope.state = {
                    options: ['default', 'uploading', 'uploaded', 'error'],
                    current: 'default',
                    msg: '',
                    set: function (state, msg) {
                        if ($scope.state.options.indexOf(state) === -1) {
                            window.console.warn('unknown state: ', state);
                        }
                        $scope.state.msg = msg || '';
                        $scope.state.current = state;
                    }
                };

                $scope.submit = function(){

                    var file = $scope.data.upload.file;

                    console.log('file is ' + JSON.stringify(file));

                    neoSimpleFileUpload.uploadFileToUrl($scope.data.upload.file, "/"+ config.cmsSlug +"/services/upload-json");
                };

                /*
                $scope.submit = function () {
                    dataViewModelFactory.get(
                        {'collections[]':$scope.selectedCollections},
                        function (response) {

                            if (response.data.collections) {
                                var hiddenElement = window.document.createElement('a');
                                hiddenElement.href = 'data:attachment/json,' + encodeURI(JSON.stringify(response.data.collections));
                                hiddenElement.target = '_blank';
                                hiddenElement.download = 'data.json';
                                hiddenElement.click();
                            }
                        }
                    );
                };

                // Init list
                dataViewModelFactory.get(
                    {},
                    function (response) {
                        $scope.data.collectionNames = response.data.collectionNames;
                        $scope.selectedCollections  = _.clone($scope.data.collectionNames);
                    }
                );
                */
            }
        ]
    );
}());

