/**
 * Created by Paul on 7/31/14.
 */

/*jslint nomen: true, white: true */
/*global NP, angular, window, _*/

(function () {

    'use strict';

    angular.module('dataApp').controller(
        'dataDownloadController',
        [
            '$scope',
            'dataViewModelFactory',
            function ($scope, dataViewModelFactory) {

                $scope.data = {};
                $scope.selectedCollections = [];

                $scope.toggleSelection = function (collectionName) {
                    var index = $scope.selectedCollections.indexOf(collectionName);
                    if (index === -1) {
                        $scope.selectedCollections.push(collectionName);
                    } else {
                        $scope.selectedCollections.splice(index, 1);
                    }
                };


                $scope.downloadCollections = function () {
                    dataViewModelFactory.get(
                        {'collections[]':$scope.selectedCollections},
                        function (response) {

                            if (response.data.collections) {
                                var hiddenElement = window.document.createElement('a');
                                hiddenElement.href = 'data:attachment/json,' + encodeURI(JSON.stringify(response.data.collections));
                                hiddenElement.target = '_blank';
                                hiddenElement.download = 'data.json';
                                hiddenElement.click();
                            }
                        }
                    );
                };



                // Init list
                dataViewModelFactory.get(
                    {},
                    function (response) {
                        $scope.data.collectionNames = response.data.collectionNames;
                        $scope.selectedCollections  = _.clone($scope.data.collectionNames);
                    }
                );


            }
        ]
    );
}());

