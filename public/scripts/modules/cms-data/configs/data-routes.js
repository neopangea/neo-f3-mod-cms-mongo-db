/*jslint nomen: true, white: true */
/*global angular*/

(function () {

    'use strict';

    angular.module('dataApp').config([
        '$routeProvider',
        function ($routeProvider) {
            $routeProvider
                .when('/download', {
                    controller: 'dataDownloadController',
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-db/public/scripts/modules/cms-data/views/data-download-view.html'
                }).when('/upload', {
                    controller: 'dataUploadController',
                    templateUrl: '/vendor/neopangea/f3-mod-cms-mongo-db/public/scripts/modules/cms-data/views/data-upload-view.html'
                }).otherwise({
                    redirectTo: '/download'
                });
        }
    ]);

}());