/*global angular*/

(function () {
    'use strict';

    angular.module('dataApp', [
        'neoCmsBaseModule',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngRoute'
    ]);

}());
